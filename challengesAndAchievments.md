# **Experience Highlights**

## 1. Built or implemented new testing tools or frameworks

**Experience:** I encountered a challenge with flaky tests due to the unreliable behavior of microservices in an API project. This inconsistency made it difficult to ensure the reliability of our testing process.

**Solution:** To address this issue, I spearheaded the development of a new testing framework called "Midlevel Testing." This framework recorded responses from the microservices and stored them in a database. Whenever these responses changed, the framework would trigger an alert, prompting us to update our code and tests accordingly.

**Impact:** The introduction of the Midlevel Testing framework significantly reduced the flakiness in our tests. With a more stable testing environment, our team gained confidence in the accuracy of our changes and the reliability of our tests.

---

## 2. Made changes to prevent defects from occurring or improve the quality before reaching the testing stage

**Experience:** During a project, I noticed that one of our components had 0% code coverage for unit testing, leaving it vulnerable to bugs and defects.

**Action:** Recognizing the importance of unit testing, I raised this issue with the development team and emphasized the need to improve code coverage.

**Result:** As a result of this initiative, the developers increased the code coverage of the component to 40%. This improvement led to a significant reduction in the number of bugs and defects identified during testing, enhancing the overall quality of our software.

---

## 3. Recognized a pattern of bugs repeating and implemented a change to stop the pattern from recurring

**Experience:** In a project reliant on an external service for metadata integration into our responses, we consistently encountered a recurring issue. The external service had a bug that caused it to provide metadata in XML format for a specific data type, which consistently disrupted our API responses.

**Observation:** Recognizing the repeated pattern of errors caused by this bug, it became evident that a proactive approach was needed to address the issue.

**Action:** I initiated discussions with the relevant stakeholders to highlight the impact of this recurring problem on our project's functionality and reliability. Additionally, I collaborated with the external service provider to communicate the issue and advocate for a resolution.

**Result:** Through concerted efforts and effective communication, the external service provider rectified the bug in their system, ensuring that metadata was consistently provided in the expected format. As a result, the pattern of errors disrupting our API responses was successfully eliminated, enhancing the reliability and stability of our project.

