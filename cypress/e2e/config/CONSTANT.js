export const ENDPOINT_PREFIX = {
  findBugs: "/find-bugs/#",
  store: "/store",
  cart: "/my-cart",
  billing: "/my-cart/?ec_page=checkout_info",
  shipping: "/my-cart/?ec_page=checkout_shipping",
  payment: "/my-cart/?ec_page=checkout_payment"
};
