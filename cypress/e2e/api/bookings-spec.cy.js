
describe('Restful Booker API Tests', () => {
    let token;
  
    before(() => {
      // Create a token
      cy.request('POST', 'https://restful-booker.herokuapp.com/auth', {
        username: 'admin',
        password: 'password123'
      }).then((response) => {
        expect(response.body).to.have.property('token');
        token = response.body.token;
        cy.log(token)
      });
    });
  
    it('should be able to get bookings', () => {
      // Get bookings
      cy.request('GET', 'https://restful-booker.herokuapp.com/booking').then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body).to.not.be.null;
      });
    });
  
    it('should be able to create a booking', () => {
      // Create a booking
      const newBooking = {
        firstname: 'John',
        lastname: 'Doe',
        totalprice: 123,
        depositpaid: true,
        bookingdates: {
          checkin: '2023-01-01',
          checkout: '2023-01-05'
        },
        additionalneeds: 'Breakfast'
      };
      
      cy.request('POST', 'https://restful-booker.herokuapp.com/booking', newBooking).then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.bookingid).to.not.be.null;
      });
    });
  
    it('should be able to update a booking', () => {
      // Update a booking
      // Assume that the booking ID to update is 1
      const updateBooking = {
        firstname: 'Jane',
        lastname: 'Doe',
        totalprice: 150,
        depositpaid: true,
        bookingdates: {
          checkin: '2023-01-10',
          checkout: '2023-01-15'
        },
        additionalneeds: 'Dinner'
      };

      cy.log(token)
  
      cy.request({
        method: 'PUT',
        url: 'https://restful-booker.herokuapp.com/booking/1',
        headers: { 'Cookie': `token=${token}`, 'Accept': 'application/json' },
        body: updateBooking
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });

    it('should be able to partially update a booking', () => {
      // Partially update a booking
      // Assume that the booking ID to update is 1
      const partialUpdateBooking = {
        firstname: 'NewName'
      };
  
      cy.request({
        method: 'PATCH',
        url: 'https://restful-booker.herokuapp.com/booking/1',
        headers: { 'Cookie': `token=${token}`, 'Accept': 'application/json' },
        body: partialUpdateBooking
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  
    it('should be able to delete a booking', () => {
      // Delete a booking
      // Assume that the booking ID to delete is 1
      cy.request({
        method: 'DELETE',
        url: 'https://restful-booker.herokuapp.com/booking/1',
        headers: { 'Cookie': `token=${token}` }
      }).then((response) => {
        expect(response.status).to.eq(201);
      });
    });
  });
  