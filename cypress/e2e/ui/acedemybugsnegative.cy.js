import BasePage from "../pages/BasePage";
import FindBugsPage from "../pages/FindBugsPage";
import CartPage from "../pages/CartPage";
import BillingInfoPage from "../pages/BillingInfoPage";

describe("Should have essential guard rails ensuring user cant perform certain actions", () => {
  let basePage;

  before(() => {
    basePage = new BasePage();
  });


  it("should not be able to continue without billing data", () => {
    FindBugsPage.open();
    FindBugsPage.addToCartNthProduct(0);
    FindBugsPage.viewCart();
    CartPage.verifyPageUrl();
    CartPage.clickCheckoutButton();

    BillingInfoPage.continueToShippingButton.eq(0).click()
    BillingInfoPage.verifyPageUrl()
  });

});
