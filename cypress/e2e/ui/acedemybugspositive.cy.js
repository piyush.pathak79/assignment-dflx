import BasePage from "../pages/BasePage";
import FindBugsPage from "../pages/FindBugsPage";
import ProductDetailPage from "../pages/ProductDetailPage";
import CartPage from "../pages/CartPage";
import BillingInfoPage from "../pages/BillingInfoPage";
import ShippingInfoPage  from "../pages/ShippingInfoPage";
import PaymentInfoPage from "../pages/PaymentInfoPage";


describe("Should have essential components and functionalities", () => {
  let basePage;

  before(() => {
    basePage = new BasePage();
  });

  it("titles and grid should be visible", () => {
    FindBugsPage.open();
    FindBugsPage.verifyTitles();
    FindBugsPage.verifyProductGrid();
  });

  it("should take to product details page on clicking any search result", () => {
    FindBugsPage.open();
    FindBugsPage.selectNthProduct(0);
    ProductDetailPage.verifyPageUrl();
  });

  it("should be able to update the sort order", () => {
    FindBugsPage.open();
    FindBugsPage.selectSortOrder('Price Low-High')
    FindBugsPage.verifySortOrder('Price High-Low')
  });

  it("should be able to update the pagination", () => {
    FindBugsPage.open();
    FindBugsPage.verifyPagination('10')
  });

  it("should be able to add to cart and verify total", () => {
    FindBugsPage.open();
    FindBugsPage.addToCartNthProduct(0);
    FindBugsPage.viewCart();
    CartPage.verifyPageUrl();
    CartPage.verifyTotalPrice();
  });

  //combine above and below test when cart amount issue is resolved

  it("should be able to process a order completely", () => {
    FindBugsPage.open();
    FindBugsPage.addToCartNthProduct(0);
    FindBugsPage.viewCart();
    CartPage.verifyPageUrl();
    CartPage.clickCheckoutButton();
    BillingInfoPage.verifyPageUrl();

    const billingData = {
      country: 'United States',
      firstName: 'John',
      lastName: 'Doe',
      companyName: 'Acme Inc.',
      address: '123 Main St',
      city: 'New York',
      state: 'NE',
      zip: '10001',
      phone: '1234567890',
      email: 'john.doe@example.com',
    };

    BillingInfoPage.fillBillingInfo(billingData)
    BillingInfoPage.continueToShippingButton.eq(0).click()
    ShippingInfoPage.verifyPageUrl();
    ShippingInfoPage.selectShippingMethod('55') //In-Store Pickup
    ShippingInfoPage.continueToPaymentButton.click()
    PaymentInfoPage.verifyPageUrl()
    PaymentInfoPage.submitOrderButton.click()
  });

});
