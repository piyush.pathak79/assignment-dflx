import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require("../config/routes");

class CartPage extends BasePage {
  get totalPrice() {
    return cy.get("div.ec_cart_price_row_total#ec_cart_total");
  }
  get shippingPrice() {
    return cy.get("#ec_cart_shipping");
  }
 
  get checkoutButton() {
    return cy.get("div.ec_cart_button_row_checkout > a.ec_cart_button.ec_cart_button_checkout")
  }

  open() {
    // cy.visit(Cypress.env('URL'));
    return super.open(ENDPOINT_PREFIX + routes);
  }

  verifyPageUrl() {
    cy.url().should("include", ENDPOINT_PREFIX.cart);
  }

  verifyTotalPrice(productPrice, shippingPrice) {
    if (!productPrice) {
      cy.get("@selectedProductPrice").then(function (price) {
        productPrice = price;
      });
    }

    if (!shippingPrice) {
      this.shippingPrice.invoke("text").then(function (price) {
        shippingPrice = price;
      });
    }

    this.totalPrice.invoke("text").then((totalPrice) => {
      // Use totalPrice variable to get the total price
      const totalPriceFloat = parseFloat(totalPrice.replace("$", ""));
      const productPriceFloat = parseFloat(productPrice.replace("$", ""));
      const shippingPriceFloat = parseFloat(shippingPrice.replace("$", ""));
      const expectedTotalPrice = productPriceFloat + shippingPriceFloat;
      expect(totalPriceFloat).to.equal(expectedTotalPrice);
    });
  }

  clickCheckoutButton() {
    this.checkoutButton.click()
  }
}

export default new CartPage();
