import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require('../config/routes');

class PaymentInfoPage extends BasePage{

    get submitOrderButton() {
        return cy.get('#ec_cart_submit_order[value="SUBMIT ORDER"]');
      }
    
    open() {
        return super.open(ENDPOINT_PREFIX + routes)
    }

    verifyPageUrl() {
        cy.url().should('include', ENDPOINT_PREFIX.payment);
    }

    selectShippingMethod(methodValue) {
        this.shippingMethodRadioButtons.check(methodValue, { force: true });
      }

}


export default new PaymentInfoPage();