import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require('../config/routes');

class ProductDetailPage extends BasePage{
    open() {
        return super.open(ENDPOINT_PREFIX + routes)
    }

    verifyPageUrl() {
        cy.url().should('include', ENDPOINT_PREFIX.store);
    }

}


export default new ProductDetailPage();