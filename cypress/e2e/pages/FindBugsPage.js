import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require("../config/routes");

class FindBugsPage extends BasePage {
    get title() {
        return cy.get(".entry-content.single-entry-content");
    }
    get productList() {
        return cy.get("ul li.ec_product_li");
    }
    get addToCart() {
        return cy.get("span.ec_product_addtocart");
    }
    get productAddedIcon() {
        return cy.get("span.ec_product_addtocart");
    }
    get sortOrder() {
        return cy.get("#sortfield");
    }
    get pagination() {
        return cy.get(".ec_product_page_perpage");
    }
    get numberOfProductShowing() {
        return cy.get("span.ec_product_page_showing")
    }

    open() {
        return super.open(ENDPOINT_PREFIX.findBugs + routes);
    }

    verifyTitles() {
        this.title.contains("Find Bugs").should("be.visible");
        this.title
            .contains(
                "Explore a practice test site that has 25 real bugs planted inside."
            )
            .should("be.visible");
    }

    verifyProductGrid() {
        this.productList.should("have.length", 18);
    }

    selectNthProduct(nth) {
        this.productList.eq(nth).click();
    }

    getNthProductTitle(nth) {
        return this.productList.eq(nth).find('.ec_product_title').invoke("text");
    }

    addToCartNthProduct(nth) {
        this.productList
            .eq(nth)
            .find(".ec_price")
            .invoke("text")
            .then((price) => {
                price = price.trim();
                cy.wrap(price).as("selectedProductPrice");
            });
        cy.log(this.selectedProductPrice);
        this.productList.eq(nth).find('a[id^="ec_add_to_cart_"]').eq(1).click();
        this.productList
            .eq(nth)
            .find(".ec_product_successfully_added_container")
            .should("have.attr", "style", "display: block;");
        this.productAddedIcon.should("exist");
    }

    viewCart() {
        this.productAddedIcon.eq(1).click();
    }

    selectSortOrder(sortOrder) {
       this.sortOrder.select(sortOrder)
    }

    verifySortOrder(sortOrder) {
        const title = this.getNthProductTitle(0)
        this.selectSortOrder(sortOrder)
        const updatedResultTitle = this.getNthProductTitle(0)
        expect(title).to.not.eql(updatedResultTitle)
    }

    verifyPagination(value) {
        this.pagination
          .contains('a.what-we-offer-pagination-link', value)
          .click();

        this.productList.its('length').then((length => {
            expect(length).to.be.eql(value)
        }))
      }
}

export default new FindBugsPage();
