import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require('../config/routes');

class BillingInfoPage extends BasePage{

    get shippingMethodRadioButtons() {
        return cy.get('input[name="ec_cart_shipping_method"]');
      }
      get continueToPaymentButton() {
        return cy.get('.ec_cart_button.ec_cart_button_shipping_next[value="CONTINUE TO PAYMENT"]');
      }  
    
    open() {
        return super.open(ENDPOINT_PREFIX + routes)
    }

    verifyPageUrl() {
        cy.url().should('include', ENDPOINT_PREFIX.shipping);
    }

    selectShippingMethod(methodValue) {
        this.shippingMethodRadioButtons.check(methodValue, { force: true });
      }

}


export default new BillingInfoPage();