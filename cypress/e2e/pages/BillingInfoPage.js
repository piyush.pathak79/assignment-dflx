import BasePage from "./BasePage";
import { ENDPOINT_PREFIX } from "../config/CONSTANT";
const routes = require('../config/routes');

class BillingInfoPage extends BasePage{
    get billingCountrySelect() {
        return cy.get('#ec_cart_billing_country');
      }
    
      get billingFirstNameInput() {
        return cy.get('#ec_cart_billing_first_name');
      }
    
      get billingLastNameInput() {
        return cy.get('#ec_cart_billing_last_name');
      }
    
      get billingCompanyNameInput() {
        return cy.get('#ec_cart_billing_company_name');
      }
    
      get billingAddressInput() {
        return cy.get('#ec_cart_billing_address');
      }
    
      get billingCityInput() {
        return cy.get('#ec_cart_billing_city');
      }
      get billingStateSelect() {
        return cy.get('#ec_cart_billing_state_US');
      }
    
      get billingZipInput() {
        return cy.get('#ec_cart_billing_zip');
      }
    
      get billingPhoneInput() {
        return cy.get('#ec_cart_billing_phone');
      }

      get emailInput() {
        return cy.get('#ec_contact_email');
      }
    
      get retypeEmailInput() {
        return cy.get('#ec_contact_email_retype');
      }

      get continueToShippingButton() {
        return cy.get('.ec_cart_button.ec_checkout_details_submit[value="CONTINUE TO SHIPPING"]');
      }

      get checkoutErrorMessage() {
        return cy.get('#ec_checkout2_error.ec_cart_error_row');
      }

    open() {
        return super.open(ENDPOINT_PREFIX + routes)
    }

    verifyPageUrl() {
        cy.url().should('include', ENDPOINT_PREFIX.billing);
    }

    fillBillingInfo(billingData) {
        this.billingCountrySelect.select(billingData.country);
        this.billingFirstNameInput.type(billingData.firstName);
        this.billingLastNameInput.type(billingData.lastName);
        this.billingCompanyNameInput.type(billingData.companyName);
        this.billingAddressInput.type(billingData.address);
        this.billingCityInput.type(billingData.city);
        this.billingCountrySelect.select(billingData.state);
        this.billingZipInput.type(billingData.zip);
        this.billingPhoneInput.type(billingData.phone);
        this.emailInput.type(billingData.email);
        this.retypeEmailInput.type(billingData.email);
      }

}


export default new BillingInfoPage();