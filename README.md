# Setting Up and Running Tests

## To set up and run the tests for this project, follow these steps:

1. Clone the repository to your local machine
2. Navigate to the project directory
3. `npm install`
4. `npm run test` for all tests
5. `npm run test:ui` for ui tests
6. `npm run test:api` for api tests


## The branch main uses the docker image directly
## The branch master uses the docker container to run the tests so check it out


# Test Scenarios

This section contains Gherkin scenarios for testing various functionalities of the application, covering both positive and negative cases.

## Feature: Catalog Page Functionalities
As a user, I want functionalities on the catalog page to make my search experience better.

### Scenario: Changing sort order
Given I am on the home page displaying the product catalog  
When I change the Sort Method from Default Sorting to "Price High - Low"  
Then the catalog should be updated accordingly

### Scenario: Changing total results per page
Given I am on the home page displaying the product catalog  
When I click on 10 for the View value for total results for pagination  
Then the catalog should be updated accordingly and only 10 products should be displayed  
And the text "Showing all n results" should be updated as n to 10

### Scenario: Changing currency
Given I am on the home page displaying the product catalog  
When I change the currency from USD to another currency, let's say "EUR"  
Then the catalog should be updated accordingly with prices reflecting the updated currency


## Feature: Shopping Cart
As a user, I want to manage items in my shopping cart so that I can proceed to purchase.

### Scenario: Adding items to the cart
Given I am on the home page displaying the product catalog  
When I click the "Add to Cart" button for a product  
Then the green banner containing "Successfully Added to your shopping cart" should be displayed on the product thumbnail  
And the banner containing the text "Product successfully added to your cart." should be displayed  
And a "View Cart" button should be displayed

### Scenario: Clicking the View Cart button
Given I have just added an item to my cart  
And I have noted the price of the added item  
When I click the "View Cart" button  
Then I should be navigated to the Checkout page  
And the Total price should be equal to the actual price from step 2 plus shipping price

### Scenario: Removing items from the cart
Given I have items in my shopping cart  
When I click the remove 'X' button preceding the item  
Then the item should be removed from the cart  
And the price should be updated accordingly

### Scenario: Updating the quantity of items in the cart
Given I have an item in my cart  
When I update the quantity to "3" and click "Update"  
Then the total price should be recalculated correctly

### Scenario: Clicking the Checkout button
Given I have an item in my cart  
When I click the "Checkout" button  
Then I should be navigated to the Billing info page

### Scenario: Clicking the Continue Shopping button
Given I have an item in my cart  
When I click the "Continue shopping" button  
Then I should be navigated back to the product catalog page

## Feature: Billing Information
As a user, I want to provide billing information to complete my purchase.

### Scenario: Entering valid billing information
Given I am on the billing info page  
When I enter valid billing information  
And I click on "Continue to shipping"  
Then I should be redirected to the shipping info page

### Scenario: Entering invalid billing information
Given I am on the billing info page  
When I enter an invalid zip code  
And I click on "Continue to shipping"  
Then I should see an error message about the invalid zip code

## Feature: Shipping Information
As a user, I want to choose a shipping method for my items.

### Scenario: Selecting a shipping method
Given I am on the shipping info page  
When I select "Priority 2 Day Shipping"  
And I click on "Continue to payment"  
Then the Shipping cost on the order detail section should be updated accordingly  
And I should be redirected to the payment page  
And the Grand Total should be reflected correctly


## Feature: Payment
As a user, I want to enter payment details to complete my purchase.

### Scenario: Entering valid payment information
Given I am on the payment page  
And Pay by Direct Deposit is selected by default  
When I click on "Submit order"  
Then I should see a confirmation message with order details

### Scenario: Entering invalid payment information
Given I am on the payment page  
When I enter invalid payment information  
And I click on "Submit order"  
Then I should see a message indicating the payment was not successful

## Feature: Product Search
As a user, I want to search for products so that I can find items to purchase.

### Scenario: Searching for a product from the cart page
Given I am on any cart section, let's say on payment  
When I search for a valid keyword like "yellow shoes" from the "PRODUCT SEARCH" input  
Then I should be redirected to the catalog page with matching products from the search term
